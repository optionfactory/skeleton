# Skeleton, a collection of instruments commonly used in Optionfactory applications

THIS WILL BE SPLIT INTO INDIVIDUAL PROJECTS

## Embedded Webserver
Package ```net.optionfactory.skeleton.web.embedded``` contains tools to serve Web Application using an embedded Undertow.

Note: web application resources can be served either from:

- *CLASSPATH*: resources are served from application jar, generally used when application is deployed. Any change to web application sources will not be propagated unless application is rebuilt and restarted.
- *FILESYSTEM*: resources are served from filesystem, generally used during development. Changes to web application sources are immediately visible when application is running.

The enum WebappSource should be injected where any resource path is defined and should be used to prefix it for the proper environment, for example:

```java
registry.addResourceHandler("/assets/**")
                .addResourceLocations(webappSource.prefix("/assets/"))
```

```java
@Bean
public ITemplateResolver pagesHtmlTemplateResolver(ApplicationContext applicationContext) {
    final SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
    templateResolver.setApplicationContext(applicationContext);
    templateResolver.setPrefix(webappSource.prefix("/WEB-INF/views/"));
    templateResolver.setSuffix(".html");
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCacheable(!development);
    templateResolver.setCharacterEncoding("UTF-8");
    return templateResolver;
}
```

### Example

Main.java
```java
public class Main {

    public static void main(String[] args) {
        final Logger logger = Logger.getLoggerFactory(Main.class);
        final AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(CoreConfig.class, DatabaseConfig.class, SecurityConfig.class, WebserverConfig.class);
        rootContext.refresh();
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                logger.info("Shutting down...");
                rootContext.close();
                rootContext.stop();
                logger.info("Shutdown completed.");
            }

        });
        
    }
    
}

```

WebserverConfig.class
```java
@Configuration
public class WebserverConfig {

    @Bean
    public WebApplication webapplication() {
        return new OurWebApplication();
    }

    @Bean
    public WebAppServletContainerInitializer servletContainerInitializer(
            AnnotationConfigWebApplicationContext rootContext,
            WebApplication webApplication
    ) {
        return new WebAppServletContainerInitializer(rootContext, webApplication);
    }

    @Bean
    public UndertowWebserver undertowServer(
            ServletContainerInitializer servletContainerInitializer,
            @Value("${webserver.listening.address}") String webserverListeningAddress,
            @Value("${webserver.listening.port}") int webserverListeningPort,
            @Value("${webserver.webapp.context}") String webserverWebappContext,
            WebappSource webappSource
    ) {
        return new UndertowWebserver(servletContainerInitializer, webserverListeningAddress, webserverListeningPort, webserverWebappContext, webappSource);
    }

    @Bean
    public WebappSource webappSource(
            @Value("${webserver.webapp.source}") String webserverWebappSource
    ) {
        return WebappSource.valueOf(webserverWebappSource);
    }

}

```

OurWebApplication.class
```java

public class OurWebApplication implements WebApplication {

    public void setup(AnnotationConfigWebApplicationContext webContext, ServletContext sc) throws ServletException {
        sc.addFilter("springSecurityFilterChain", new DelegatingFilterProxy()).addMappingForUrlPatterns(null, false, "/*");
        sc.addListener(new ContextLoaderListener(webContext));

        final AnnotationConfigWebApplicationContext apiContext = new AnnotationConfigWebApplicationContext();
        apiContext.setParent(webContext);
        apiContext.setDisplayName("apiContext");
        apiContext.register(InternalApiConfig.class);
        final ServletRegistration.Dynamic api = sc.addServlet("api", new JsonDispatcherServlet(apiContext));
        api.setLoadOnStartup(1);
        api.setMultipartConfig(new MultipartConfigElement("/tmp", 10 * 1024 * 1024, 12 * 1024 * 1024, 0));
        api.addMapping("/api/*");

        final AnnotationConfigWebApplicationContext pagesContext = new AnnotationConfigWebApplicationContext();
        pagesContext.setParent(webContext);
        pagesContext.setDisplayName("PagesContext");
        pagesContext.register(PagesConfig.class, ControllersExceptionHandlers.class);
        final ServletRegistration.Dynamic pages = sc.addServlet("pages", new DispatcherServlet(pagesContext));
        pages.setLoadOnStartup(1);
        pages.addMapping("/");

        final AnnotationConfigWebApplicationContext staticContext = new AnnotationConfigWebApplicationContext();
        staticContext.setParent(webContext);
        staticContext.setDisplayName("staticContext");
        staticContext.register(StaticConfig.class);
        final ServletRegistration.Dynamic statics = sc.addServlet("static", new DispatcherServlet(staticContext));
        statics.setLoadOnStartup(1);
        statics.addMapping("/robots.txt", "*.css", "*.map", "*.js", "*.png", "*.gif", "*.jpg", "*.svg", "*.ico", "*.woff", "*.woff2", "*.ttf", "*.eot", "*.webm", "*.mp4", "*.pdf");
    }

}

```

project.properties
```
...

webserver.listening.address=0.0.0.0
webserver.listening.port=8080
webserver.webapp.context=/
webserver.webapp.source=CLASSPATH

...

```

pom.xml
```xml
...

<plugin>
    <artifactId>maven-resources-plugin</artifactId>
    <version>3.1.0</version>
    <configuration>
        <useDefaultDelimiters>false</useDefaultDelimiters>
        <delimiters>
            <delimiter>@{*}</delimiter>
        </delimiters>
    </configuration>
    <executions>
        <execution>
            <id>webapp</id>
            <phase>generate-resources</phase>
            <goals>
                <goal>copy-resources</goal>
            </goals>
            <configuration>
                <outputDirectory>${project.basedir}/target/classes/</outputDirectory>
                <resources>
                    <resource>
                        <directory>src/main/webapp</directory>
                        <filtering>false</filtering>
                    </resource>
                </resources>
            </configuration>
        </execution>
    </executions>
</plugin>
...

```


