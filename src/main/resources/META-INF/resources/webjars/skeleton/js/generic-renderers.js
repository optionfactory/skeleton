var tooltipRenderer = function (data, cell, record, rowIndex, columnIndex, store) {
    if (!!data) {
        cell.tdAttr += ' data-qtip="' + Ext.String.htmlEncode(data) + '"';
    }
    return data;
}
var tooltipWrapperRenderer = function (rendererFunction) {
    return function () {
        var toDisplay = rendererFunction.apply(this, arguments);
        return tooltipRenderer.apply(this, [toDisplay].concat(Array.prototype.slice.call(arguments, 1)));
    }
}
var nullableRenderer = function (rendererFunction) {
    return function (data) {
        if (!data) {
            return "N/A";
        }
        return rendererFunction.apply(this, arguments);
    }
}
var twoDigitsNumberRenderer = function (data, cell, record, rowIndex, columnIndex, store) {
    return Ext.util.Format.number(data, "0.00");
};
var percentRenderer = function (data, cell, record, rowIndex, columnIndex, store) {
    return (data >= 0 ? "+" : "") + Ext.util.Format.number(data, "0.00%")
};
var renderListAsTable = function (list) {
    var header = '<table>'
    var body = '';
    for (var i = 0; i != list.length; ++i) {
        body += '<tr><td>' + list[i] + '</td></tr>';
    }
    var footer = '</table>';
    return header + body + footer;
}
var renderObjectDetailsAsTable = function (obj) {
    var header = '<table>'
    var body = '';
    for (var propt in obj) {
        body += '<tr><td>' + propt + '</td><td>' + obj[propt] + '</td></tr>';
    }
    var footer = '</table>';
    return header + body + footer;
}
var errorsToText = function (errors) {
    return "<ul>" + (errors || []).map(function (e) {
        return Ext.String.format("<li><b>{0}</b>{1}{2}</li>", e.context && (Ext.htmlEncode(e.context) + ":"), Ext.htmlEncode(e.reason), e.details && (" - " + Ext.htmlEncode(e.details)));
    }).join("\n") + "</ul>";
}
