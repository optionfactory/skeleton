Ext.define('OptionFactory.DetailsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.opfa-detailsGrid',
    constructor: function (conf) {
        var defaultConf = {
            viewConfig: {
                stripeRows: true,
                enableTextSelection: true
            },
            enableColumnHide: false,
            hideHeaders: true,
            store: Ext.create('Ext.data.Store', {
                fields: ['key', 'value'],
                sorters: conf.sorters || [{
                        property: 'key',
                        direction: 'ASC'
                    }],
                data: conf.data || []}),
            columns: [
                {text: 'Field', tooltip: 'Field', dataIndex: 'key', flex: conf.keyFlex || 1, renderer: tooltipRenderer},
                {text: 'Value', tooltip: 'Value', dataIndex: 'value', flex: conf.valueFlex || 2, renderer: function (data, cell, record, rowIndex, columnIndex, store) {
                        if (!record.data.renderer || (typeof record.data.renderer !== 'function')) {
                            return tooltipRenderer(data, cell, record, rowIndex, columnIndex, store);
                        }
                        return record.data.renderer(data, cell, record, rowIndex, columnIndex, store);
                    }
                },
                {text: 'Actions', tooltip: 'Actions', dataIndex: 'key', width: 70, xtype: 'actioncolumn', align: 'center', hidden: !conf.actions, items: conf.actions}
            ]};
        this.callParent([Ext.merge({}, defaultConf, conf)]);
        if (conf.object) {
            this.loadObject(conf.object);
        }
    }, privates: {
        loadData: function (data) {
            this.getStore().loadData(data);
        },
        loadObject: function (obj) {
            var data = [];
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    data.push({key: key, value: obj[key]});
                }
            }
            this.getStore().loadData(data);
        }
    }
});

