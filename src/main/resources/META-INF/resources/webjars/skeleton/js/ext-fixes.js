Ext.define('TreeList', {
    override: 'Ext.list.TreeItem',
    updateNode: function (node) {
        var qtip = node && node.get('qtip');
        this.callParent(arguments);
        qtip && this.element.dom.setAttribute('data-qtip', qtip);
    }
});
Ext.define('Overrides.data.PageMap', {
    override: 'Ext.data.PageMap',
    hasRange: function (start, end) {
        var pageNumber = this.getPageFromRecordIndex(start),
                endPageNumber = this.getPageFromRecordIndex(end);
        for (; pageNumber <= endPageNumber; pageNumber++) {
            if (!this.hasPage(pageNumber)) {
                return false;
            }
        }
        return true;
    }
});

