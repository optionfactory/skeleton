Ext.define('OptionFactory.DownloadButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.opfa-downloadButton',
    constructor: function (config) {
        this.callParent([config]);
        this.downloadEl = document.createElement('a');
        this.downloadEl.setAttribute("type", "hidden");
        this.changeData(config.data);
        document.body.appendChild(me.downloadEl);
        this.on('click', function () {
            this.downloadEl.click();
            this.downloadEl.remove();
        }, this);
    },
    changeData: function (data) {
        if (!data && this.disableOnEmpty) {
            this.disable();
            return;
        }
        data = data || {
        };
        this.enable();
        this.downloadEl.setAttribute('href', data.url);
        this.downloadEl.setAttribute('download', data.name);
    }
});