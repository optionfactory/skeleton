/*
 * USAGE:
 * On a form field, add the plugin:
    {
        allowBlank: false,
        fieldLabel: 'Field to validate remotely',
        name: 'remotelyValidatedFieldName',
        plugins: {
            remotevalidation: {
                remoteUrl: 'api/[... validation api url here ... ]',
                extractPayload: function(field) {
                    var payload = {  //CHANGE this to the form of your request payload
                        text: field.getValue();
                        };
                    return payload;
                }.bind(this)
            }
        }
    }

Backend controller (mock):

    import net.optionfactory.skeleton.web.remotevalidation.ValidationResponse;
    import org.springframework.web.bind.annotation.PostMapping;
    import org.springframework.web.bind.annotation.RequestBody;
    import org.springframework.web.bind.annotation.RestController;

    @RestController
    public class ExampleController {

        public static class SimpleRemoteValidationRequest {

            public String text;
        }

        @PostMapping("remoteValidationUrlHere")
        public ValidationResponse validateAcwlGroupGatewayName(@RequestBody SimpleRemoteValidationRequest request) {
            if (request == null || request.text == null || request.text.isEmpty()) {
                return ValidationResponse.error("Empty or null request");
            }
            return ValidationResponse.valid();
        }

    }

 */

Ext.define('OptionFactory.plugin.RemoteValidation', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.remotevalidation',
    remoteValid: false,
    remoteReason: '',
    pending: 0,
    remoteUrl: '',
    responseField: 'valid',
    reasonField: 'reason',
    init: function (field) {
        var self = this;
        this.setCmp(field);
        field.enableKeyEvents = true;
        field.on('keyup', this.revalidateField, this, {buffer: 500});
        Ext.override(field, {
            isValid: function (preventMark) {
                return this.callParent([preventMark]) && self.pending === 0 && self.remoteValid;
            },
            setValue: function (value) {
                var previousValue = this.value;
                this.callParent([value]);
                if (!value) {
                    self.revalidateField();
                }
                if (previousValue !== value) {
                    self.revalidateField();
                }
            },
            getErrors: function () {
                var errors = this.callParent();
                if (!self.remoteValid) {
                    errors.push(self.remoteReason);
                }
                return errors;
            }
        });
    },
    destroy: function () {
        // remove event handlers
        this.setCmp(null);
    },
    success: function (field, trigger, response) {
        return 'empty-trigger';
    },
    failure: function (field, trigger, response) {
        return 'empty-trigger';
    },
    extractPayload: function (field) {
        return JSON.stringify(field.getValue());
    },
    revalidateField: function () {
        var self = this;
        this.pending += 1;
        if (this.pending > 1) {
            console.log('validation pending, quitting early')
            return;
        }
        this.remoteValid = false;
        this.remoteReason = 'Checking...';
        this.getCmp().markInvalid('Checking...');
        this.validationPayload = this.extractPayload(this.getCmp());
        this.getCmp().fireEvent('validitychange', this.getCmp(), false);
        setTimeout(function () {
            Ext.Ajax.request({
                url: self.remoteUrl,
                method: 'POST',
                jsonData: self.validationPayload,
                success: function (r) {
                    var result = JSON.parse(r.responseText);
                    console.log('validation success')
                    self.updateValidation(result[self.responseField], result[self.reasonField], result);
                    self.getCmp().up('form').isValid()
                },
                failure: function (r) {
                    var errors = JSON.parse(r.responseText);
                    console.log('validation failure')
                    var reason = errors.map(e => e.reason).join("; ");
                    var data = {};
                    data[self.reasonField] = reason;
                    self.updateValidation(false, reason, data);
                }
            }
            );
        }, 350);

    },
    privates: {
        updateValidation: function (valid, reason, result) {
            if (this.pending !== 1 && !Ext.Object.equals(this.validationPayload, this.extractPayload())) {
                this.pending = 0;
                this.revalidateField();
                return;
            }
            var field = this.getCmp();
            this.pending = 0;
            this.remoteValid = valid;
            this.remoteReason = reason;
            if (!valid) {
                field.markInvalid(result.reason);
                //field.validate();
                field.fireEvent('validitychange', field, valid);
                return;
            }
            field.clearInvalid();
            //field.validate();
            field.fireEvent('validitychange', field, valid);
        }
    }
});