Ext.define('optionfactory.AjaxProxyWithExceptionHandler', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.ajaxex',
    statics: {
        exceptionHandler: function (errors, status, statusText, operation) {
            console.log("exception ", errors, status, statusText, operation);
        },
        unauthorizedHandler: Ext.emptyFn,
    },
    constructor: function (config) {
        var defaults = {
        };
        this.callParent([Ext.merge(defaults, config)]);
        this.on('exception', this._handleException, this)
    },
    _handleException: function (self, response, operation, eOpts) {
        if (operation.error.status === -1) {
            return;
        }
        optionfactory.AjaxProxyWithExceptionHandler.exceptionHandler(
                this._responseAsErrors(response, operation),
                operation.error.status,
                operation.error.status === 0 ? "Connection error" : operation.error.statusText,
                response,
                operation
                );
    },
    _responseAsErrors: function (response, operation) {
        switch (operation.error.status) {
            case 0:
            case 404:
                return [{type: "CONNECTION_ERROR", context: null, reason: "Unable to connect to server", details: null}];
            case 401:
                return [{type: "TENANT_ERROR", context: null, reason: "Unauthorized: Your session has expired", details: null}];
            case 403:
                return [{type: "TENANT_ERROR", context: null, reason: "Forbidden: access to this resource has been denied", details: null}];
            default:
            try {
                return JSON.parse(response.responseText);
            } catch (ex) {
                return [{type: 'UNPARSEABLE_ERROR', context: null, reason: "Received an unparseable error from server", details: null}];
            }
        }
    }
});