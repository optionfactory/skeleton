/*
 * Allows to treat a grid as a form field, with minimal validation on the number of elements
 * 
 * Example usage: 
 * 1) create a custom field, for example:
 * 
    Ext.define('OptionFactory.skeleton.examples.GridAsField', {
        extend: 'OptionFactory.form.field.GridField',
        alias: 'widget.customGridAsField',
        constructor: function (conf) {
            var me = this;
            var defaultConf = {
                msgTarget: 'under',
                grid: {
                    flex: 1,
                    title: 'Users',
                    header: {
                        titlePosition: 1,
                        items: [
                            {
                                iconCls: 'fas fa-plus text-green',
                                handler: function () {
                                    //show a form to the user to fill row details (or something like that), and then...:
                                    var newRow = { 
                                        name:"test", 
                                        surname:"user"
                                    };
                                    me.addRow(newRow);
                                },
                                tooltip: "Add user"
                            }
                        ]
                    },
                    viewConfig: {
                        stripeRows: true,
                        enableTextSelection: true
                    },
                    enableColumnHide: false,
                    store: {
                        fields: [
                            'name',
                            'surname'
                        ],
                        data: []
                    },
                    columns: [
                        { text: "Name", tooltip: "Name", dataIndex: 'name', flex: 1, renderer: tooltipRenderer, filter: 'string' },
                        { text: "Surname", tooltip: "Surname", dataIndex: 'surname', flex: 1, renderer: tooltipRenderer, filter: 'string' },
                        {
                            text: "Actions", tooltip: "Actions", dataIndex: 'key', width: 60, xtype: 'actioncolumn', align: 'center', items: [
                                {
                                    xtype: 'button',
                                    getClass: function (value, meta, record) {
                                        return 'fas fa-minus text-red';
                                    },
                                    'tooltip': "Remove row",
                                    handler: function (grid, rowIndex, colIndex) {
                                        me.removeRow(rowIndex);
                                    }
                                }
                            ]
                        }
                    ]
                }
            };
            this.callParent([Ext.merge({}, defaultConf, conf)]);
        },
        setValue: function (value) {
            var data = value; //adapt each value here 
            this.callParent([data]);
        }
    });

 * 2)Use it as a form field:
 * [...]
    {
        xtype: "customGridAsField",
        height: 270,
        containsAtLeast: 1, //default 0
        containsAtMost:  3, //default undefined (as in: no limit)
        name: 'formFieldNameHere'
    }
 * [...]
 */

Ext.define('OptionFactory.form.field.AbstractField', {
    extend: 'Ext.container.Container',
    mixins: {
        labelable: 'Ext.form.Labelable',
        field: 'Ext.form.field.Field'
    },
    requires: 'Ext.layout.component.field.FieldContainer',
    alias: 'widget.abstractfield',
    componentLayout: 'fieldcontainer',
    componentCls: Ext.baseCSSPrefix + 'form-fieldcontainer',
    shrinkWrap: true,
    autoEl: {
        tag: 'div',
        role: 'presentation'
    },
    childEls: [
        'containerEl'
    ],
    combineLabels: false,
    labelConnector: ', ',
    combineErrors: false,
    maskOnDisable: false,
    invalidCls: '',
    fieldSubTpl: [
        '<div id="{id}-containerEl" data-ref="containerEl" class="{containerElCls}"',
        '<tpl if="ariaAttributes">',
        '<tpl foreach="ariaAttributes"> {$}="{.}"</tpl>',
        '<tpl else>',
        ' role="presentation"',
        '</tpl>',
        '>',
        '{%this.renderContainer(out,values)%}',
        '</div>'
    ],
    initComponent: function () {
        var me = this;
        // Init mixins
        me.initLabelable();
        me.initField();
        me.callParent();
    },
    onAdd: function (labelItem) {
        var me = this;
        // Fix for https://sencha.jira.com/browse/EXTJSIV-6424 Which was *sneakily* fixed fixed in version 37 
        // In FF < 37, positioning absolutely within a TD positions relative to the TR! 
        // So we must add the width of a visible, left-aligned label cell to the x coordinate. 
        if (labelItem.isLabelable && Ext.isGecko && Ext.firefoxVersion < 37 && me.layout.type === 'absolute' && !me.hideLabel && me.labelAlign !== 'top') {
            labelItem.x += (me.labelWidth + me.labelPad);
        }
        me.callParent(arguments);
        if (labelItem.isLabelable && me.combineLabels) {
            labelItem.oldHideLabel = labelItem.hideLabel;
            labelItem.hideLabel = true;
        }
        me.updateLabel();
    },
    onRemove: function (labelItem, isDestroying) {
        var me = this;
        me.callParent(arguments);
        if (!isDestroying) {
            if (labelItem.isLabelable && me.combineLabels) {
                labelItem.hideLabel = labelItem.oldHideLabel;
            }
            me.updateLabel();
        }
    },
    initRenderData: function () {
        var me = this;
        var data = me.callParent();
        data.containerElCls = me.containerElCls;
        data = Ext.applyIf(data, me.getLabelableRenderData());
        if (me.labelAlign === 'top' || me.msgTarget === 'under') {
            data.extraFieldBodyCls += ' ' + Ext.baseCSSPrefix + 'field-container-body-vertical';
        }
        data.tipAnchorTarget = me.id + '-containerEl';
        return data;
    },
    getFieldLabel: function () {
        var label = this.fieldLabel || '';
        if (!label && this.combineLabels) {
            label = Ext.Array.map(this.query('[isFieldLabelable]'), function (field) {
                return field.getFieldLabel();
            }).join(this.labelConnector);
        }
        return label;
    },
    getSubTplData: function () {
        var ret = this.initRenderData();
        Ext.apply(ret, this.subTplData);
        return ret;
    },
    getSubTplMarkup: function (fieldData) {
        var me = this,
            tpl = me.lookupTpl('fieldSubTpl'),
            html;
        if (!tpl.renderContent) {
            me.setupRenderTpl(tpl);
        }

        html = tpl.apply(me.getSubTplData(fieldData));
        return html;
    },
    updateLabel: function () {
        var me = this,
            label = me.labelEl;
        if (label) {
            me.setFieldLabel(me.getFieldLabel());
        }
    },
    onRender: function () {
        this.callParent(arguments);
        this.mixins.labelable.self.initTip();
        this.renderActiveError();
    },
    onFieldErrorChange: function () {
        if (this.combineErrors) {
            var me = this,
                oldError = me.getActiveError(),
                invalidFields = Ext.Array.filter(me.query('[isFormField]'), function (field) {
                    return field.hasActiveError();
                }),
                newErrors = me.getCombinedErrors(invalidFields);
            if (newErrors) {
                me.setActiveErrors(newErrors);
            } else {
                me.unsetActiveError();
            }

            if (oldError !== me.getActiveError()) {
                me.updateLayout();
            }
        }
    },
    getCombinedErrors: function (invalidFields) {
        var errors = [],
            f,
            fLen = invalidFields.length,
            field,
            activeErrors, a, aLen,
            error, label;
        for (f = 0; f < fLen; f++) {
            field = invalidFields[f];
            activeErrors = field.getActiveErrors();
            aLen = activeErrors.length;
            for (a = 0; a < aLen; a++) {
                error = activeErrors[a];
                label = field.getFieldLabel();
                errors.push((label ? label + ': ' : '') + error);
            }
        }

        return errors;
    },
    isValid: function () {
        var me = this,
            disabled = me.disabled,
            validate = me.forceValidation || !disabled;
        return validate ? me.validateValue(this.value) : disabled;
    },
    validateValue: function (value) {
        var me = this,
            errors = me.getErrors(value),
            isValid = Ext.isEmpty(errors);
        if (!me.preventMark) {
            if (isValid) {
                me.clearInvalid();
            } else {
                me.markInvalid(errors);
            }
        }

        return isValid;
    },
    markInvalid: function (errors) {
        // Save the message and fire the 'invalid' event 
        var me = this,
            oldMsg = me.getActiveError(),
            active;
        me.setActiveErrors(Ext.Array.from(errors));
        active = me.getActiveError();
        if (oldMsg !== active) {
            me.setError(active);
            if (!me.ariaStaticRoles[me.ariaRole] && me.inputEl) {
                me.inputEl.dom.setAttribute('aria-invalid', true);
            }
        }
    },
    clearInvalid: function () {
        // Clear the message and fire the 'valid' event 
        var me = this,
            hadError = me.hasActiveError();
        delete me.hadErrorOnDisable;
        me.unsetActiveError();
        if (hadError) {
            me.setError('');
            if (!me.ariaStaticRoles[me.ariaRole] && me.inputEl) {
                me.inputEl.dom.setAttribute('aria-invalid', false);
            }
        }
    },
    setError: function (error) {
        var me = this,
            msgTarget = me.msgTarget,
            prop;
        if (me.rendered) {
            if (msgTarget === 'title' || msgTarget === 'qtip') {
                prop = msgTarget === 'qtip' ? 'data-errorqtip' : 'title';
                me.getActionEl().dom.setAttribute(prop, error || '');
            } else {
                me.updateLayout();
            }
        }
    },
    renderActiveError: function () {
        var me = this,
            hasError = me.hasActiveError(),
            invalidCls = me.invalidCls + '-field';
        if (me.inputEl) {
            // Add/remove invalid class 
            me.inputEl[hasError ? 'addCls' : 'removeCls']([
                invalidCls, invalidCls + '-' + me.ui
            ]);
        }
        me.mixins.labelable.renderActiveError.call(me);
    },
    privates: {
        applyTargetCls: function (targetCls) {
            var containerElCls = this.containerElCls;
            this.containerElCls = containerElCls ? containerElCls + ' ' + targetCls : targetCls;
        },
        getTargetEl: function () {
            return this.containerEl;
        },
        initRenderTpl: function () {
            var me = this;
            if (!me.hasOwnProperty('renderTpl')) {
                me.renderTpl = me.lookupTpl('labelableRenderTpl');
            }
            return me.callParent();
        }
    }
});
Ext.define("OptionFactory.form.field.GridField", {
    extend: 'OptionFactory.form.field.AbstractField',
    alias: 'widget.opfa-grid-field',
    initComponent: function () {
        this.callParent();
        if (this.allowBlank === false) {
            this.beforeLabelTextTpl = '<span style="color:red; font-weight:bold;" data-qtip="required">* </span>';
        }
    },
    constructor: function (conf) {
        var copy = Ext.merge({}, conf);
        var gridConf = copy.grid;
        delete copy.grid;
        var defaultGridConf = {
            xtype: 'grid',
            style: {
                borderColor: '#d0d0d0',
                borderStyle: 'solid',
                borderWidth: '1px'
            },
            flex: 1
        };
        var mergedGridConf = Ext.merge({}, defaultGridConf, gridConf);
        var defaultConf = {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [mergedGridConf],
            value: [],
            containsAtLeast: 0,
            containsAtMost: null
        };
        this.callParent([Ext.merge({}, defaultConf, copy)]);
        this.on('afterrender', this.updateGrid, this);
        var roweditor = this.down('grid').findPlugin('rowediting');
        if (roweditor) {
            roweditor.on('edit', this.updateValue, this);
        }
        var celleditor = this.down('grid').findPlugin('cellediting');
        if (celleditor) {
            celleditor.on('edit', this.updateValue, this);
        }
    },
    getValidation: function () {
        if (!Ext.isEmpty(this.containsAtLeast) && this.value.length < this.containsAtLeast) {
            return `Field requires at least ${this.containsAtLeast} elements`;
        }
        if (!Ext.isEmpty(this.containsAtMost) && this.value.length > this.containsAtMost) {
            return `Field requires no more than ${this.containsAtMost} elements`;
        }
    },
    getContainsAtLeast: function () {
        return this.containsAtLeast;
    },
    setContainsAtLeast: function (containsAtLeast) {
        this.containsAtLeast = containsAtLeast;
        this.validate();
    },
    getContainsAtMost: function () {
        return this.containsAtMost;
    },
    setContainsAtMost: function (containsAtMost) {
        this.containsAtMost = containsAtMost;
        this.validate();
    },
    setValue: function (value) {
        this.value = value;
        this.checkChange();
        this.updateGrid();
        return this;
    },
    addRow: function (row) {
        var record = this.getStore().model.create(row);
        this.getStore().addSorted(record);
        this.value = this.getStore().getRange().map(r => r.data);
        this.checkChange();
        return record;
    },
    getStore: function () {
        return this.down('grid').getStore();
    },
    privates: {
        updateValue: function () {
            this.value = this.getStore().getRange().map(r => r.data);
            this.checkChange();
        },
        updateGrid: function () {
            if (!this.rendered) {
                return;
            }
            this.getStore().loadData(this.value, false);
            this.checkChange();
        },
        removeRow: function (rowIndex) {
            this.getStore().removeAt(rowIndex);
            this.value = this.getStore().getRange().map(r => r.data);
            this.checkChange();
        }

    }
});
