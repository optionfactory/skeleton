Ext.define('OptionFactory.JsonTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.opfa-jsonTree',
    constructor: function (conf) {
        var me = this;
        var defaultConf = {
            useArrows: true,
            setSelectOnExpander: false,
            viewConfig: {
                stripeRows: true,
                enableTextSelection: true
            },
            store: Ext.create('Ext.data.TreeStore', {root: !!conf.root ? mapJsonToTree(conf.root) : {}}),
            rootVisible: false,
            columns: [{
                    xtype: 'treecolumn',
                    text: 'Node',
                    tooltip: 'Node',
                    dataIndex: 'text',
                    flex: conf.nodeFlex || 2,
                }, {
                    text: 'Value',
                    tooltip: 'Value',
                    dataIndex: 'value',
                    flex: conf.valueFlex || 1,
                    renderer: tooltipRenderer
                }
            ]
        }
        this.callParent([Ext.merge({}, defaultConf, conf)]);
    },
    privates: {
        updateData: function (data) {
            if (!data) {
                this.getStore().setRoot({});
                this.disable();
            } else {
                this.getStore().setRoot(mapJsonToTree(data));
                this.enable();
            }
            this.setLoading(false);
        }
    }
});
var mapJsonToTree = function (obj) {
    function mapNode(node) {
        var keys = [];
        for (var key in node) {
            if (node.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys.map(function(k) {
            if (Array.isArray(node[k])) {
                return {text: k, expanded: true, leaf: false, children: node[k].map(function (v, i) {
                        if (typeof v !== 'object') {
                            return {text: i + 1, leaf: true, value: v};
                        } else {
                            return {text: i + 1, expanded: true, leaf: false, children: mapNode(v)};
                        }
                    })}
            } else if (typeof node[k] === 'object') {
                return {text: k, expanded: true, leaf: false, children: mapNode(node[k])}
            } else {
                return {text: k, leaf: true, value: node[k]};
            }
        });
    }
    return {expanded: true, leaf: false, children: mapNode(obj)}
}
