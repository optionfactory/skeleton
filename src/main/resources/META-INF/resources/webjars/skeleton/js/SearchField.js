Ext.define('OptionFactory.SimpleSearchField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.opfa-searchfield',
    style: 'display: table',
    debounceDelay: 500,
    flex: 1,
    triggers: {
        clear: {
            weight: 0,
            cls: Ext.baseCSSPrefix + 'form-clear-trigger',
            hidden: true,
            handler: 'onClearClick',
            scope: 'this'
        },
        search: {
            weight: 1,
            cls: Ext.baseCSSPrefix + 'form-search-trigger',
            handler: 'onSearchClick',
            scope: 'this'
        }
    },
    initComponent: function () {
        this.on('specialkey', function (f, e) {
            if (e.getKey() === e.ENTER) {
                this.onSearchClick();
            }
        }, this, {delay: this.debounceDelay});
        if (this.autoLoad) {
            this.on({
                change: {
                    fn: this.onSearchClick,
                    scope: this,
                    buffer: this.debounceDelay
                }
            });
        }
    },
    onClearClick: function () {
        var me = this;
        me.setValue('');
        me.getTrigger('clear').hide();
        me.updateLayout();
        me.fireEvent('querychanged', null);
    },
    onSearchClick: function () {
        var me = this;
        var value = me.getValue();
        if (value && value.trim().length > 0) {
            me.getTrigger('clear').show();
        } else {
            me.getTrigger('clear').hide();
            value = null;
        }
        me.updateLayout();
        me.fireEvent('querychanged', value);
    },
    bindToGridCustomFunction: function (grid, fieldName, filterFn) {
        var me = this;
        this.on('querychanged', function (value) {
            if (!!value) {
                me.activeFilter = new Ext.util.Filter({
                    property: fieldName,
                    value: value,
                    anyMatch: true,
                    filterFn: filterFn
                });
                grid.getStore().getFilters().add(me.activeFilter);
            } else if (!!me.activeFilter) {
                grid.getStore().getFilters().remove(me.activeFilter);
            }
        });
    },
    bindToGrid: function (grid, fieldName) {
        return this.bindToGridCustomFunction(grid, fieldName, null);
    }
});
