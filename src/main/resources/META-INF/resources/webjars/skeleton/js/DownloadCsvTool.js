Ext.define('OptionFactory.DownloadCsvTool', {
    extend: 'Ext.panel.Tool',
    alias: 'widget.opfa-downloadCsvTool',
    constructor: function (conf) {
        var me = this;
        if (!conf.grid) {
            throw "must specify parameter 'grid'";
        }
        var defaultConf = {
            iconCls: 'fa fa-download',
            handler: function () {
                me.downloadEl = document.createElement('a');
                me.downloadEl.setAttribute("type", "hidden");
                me.downloadEl.setAttribute('href', me.getUrl());
                me.downloadEl.setAttribute('download', me.getFileName());
                document.body.appendChild(me.downloadEl);
                me.downloadEl.click();
                me.downloadEl.remove();
            }.bind(this),
            tooltip: 'Export to CSV',
            getUrl: function () {
                var serializedFilters = conf.grid.getStore().getFilters().items.map(function (s) {
                    return s.serialize();
                });
                var serializedSorters = conf.grid.getStore().getSorters().items.map(function (s) {
                    return s.serialize();
                });
                var downloadUrl = Ext.String.format("{0}/csv?filters={1}&sorters={2}",
                        conf.baseUrl ? conf.baseUrl : conf.grid.getStore().getProxy().getUrl(),
                        encodeURI(JSON.stringify(serializedFilters)),
                        encodeURI(JSON.stringify(serializedSorters))
                        );
                if (!!jstz) {
                    return Ext.String.format("{0}&timeZone={1}", downloadUrl, encodeURI(jstz.determine().name()));
                }
                return downloadUrl;
            },
            getFileName: function () {
                return moment().format('YYYYMMDD[H]HHmm') + '-export.csv';
            }
        };
        this.callParent([Ext.merge({}, defaultConf, conf)]);
    }
});