Ext.define('OptionFactory.InfoBox', {
    extend: 'Ext.container.Container',
    alias: 'widget.opfa-infoBox',
    constructor: function (conf) {
        var defaultConf = {
            padding: 5,
            tpl: new Ext.XTemplate(
                    '<tpl if="{tooltip}">',
                    '<div data-qtip="{tooltip}" class="infoBox">',
                    '<tpl else>',
                    '<div data-qtip="{title}: {text}" class="infoBox">',
                    '</tpl>',
                    '  <span class="infoBox-icon {boxCls}"><i class="fa fa-{icon}"></i></span>',
                    '    <div class="infoBox-content">',
                    '        <span class="infoBox-text">{title}</span>',
                    '        <span class="infoBox-number">{text}</span>',
                    '    </div>',
                    '</div>'),
            data: conf
        };
        this.callParent([Ext.merge({}, defaultConf, conf)]);
    },
    updateContents: function (newData) {
        this.update(Ext.merge({}, this.data, newData));
    }
});