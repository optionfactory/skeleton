/*These require:
 <script src="${cp}/webjars/momentjs/2.19.1/moment.js"></script>
 <script src="${cp}/webjars/momentjs/2.19.1/locale/it.js"></script>
 <script src="${cp}/webjars/moment-timezone/0.5.5/moment-timezone-with-data.js"></script>
 <script src="${cp}/webjars/jstimezonedetect/1.0.6/jstz.js"></script>
 */

var buildMoment = function (v) {
    if (typeof v === 'string') {
        return moment(v);
    }
    if (v > 10000000000) { // is 'v' in Seconds?
        return moment(v);
    }
    return moment.unix(v);
};
var renderDate = function (u) {
    return u ? buildMoment(u).format("YYYY-MM-DD") : null;
}
var renderDateTime = function (u) {
    return u ? buildMoment(u).tz(jstz.determine().name()).format("YYYY-MM-DD HH:mm:ss z") : null;
}
var renderDateTimeWithDelta = function (data, cell, record, rowIndex, columnIndex, store) {
    if (!!data) {
        cell.tdAttr += ' data-qtip="' + renderDateTime(data) + ' (' + renderDeltaDateTime(data) + ')"';
    }
    return renderDateTime(data);
}
var renderLocalDateWithDelta = function (data, cell, record, rowIndex, columnIndex, store) {
    if (!data) {
        return "";
    }
    var m = moment(new Date(data));
    if (cell) {
        cell.tdAttr += ' data-qtip="' + m.format("YYYY-MM-DD") + ' (' + m.endOf("day").fromNow() + ')"';
    }
    return m.format("YYYY-MM-DD");
}

var renderDeltaDateTime = function (u) {
    return u ? buildMoment(u).fromNow() : null;
}

var renderDeltaTimeWithThreshold = function (entity, unitOfMeasure) {
    return function (data, meta, record) {
        if (!data) {
            meta.tdAttr += ' data-qtip="Never"';
            return "Never";
        }
        var instant = buildMoment(data);
        meta.tdAttr += ' data-qtip="' + Ext.String.htmlEncode(instant.format("YYYY-MM-DD HH:mm:ss z")) + '"';
        var threshold = moment().subtract(entity, unitOfMeasure);
        if (instant.isBefore(threshold)) {
            meta.style = "background-color:red;";
        }
        return instant.fromNow();
    }
}
var renderDeltaTime = function (data, meta, record) {
    if (!data) {
        meta.tdAttr += ' data-qtip="Never"';
        return "Never";
    }
    var instant = buildMoment(data);
    meta.tdAttr += ' data-qtip="' + Ext.String.htmlEncode(instant.format("YYYY-MM-DD HH:mm:ss z")) + '"';
    return instant.fromNow();
}