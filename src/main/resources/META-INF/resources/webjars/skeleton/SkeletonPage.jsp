<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="page" %>
<c:set var="cp" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
    <head>
        <title>Skeleton example</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="${cp}/webjars/extjs/6.5.3/build/classic/theme-crisp/resources/theme-crisp-all.css"/>
        <link rel="stylesheet" href="${cp}/webjars/extjs/6.5.3/build/packages/charts/classic/crisp/resources/charts-all-debug.css"/>
        <script src="${cp}/webjars/extjs/6.5.3/build/ext-all-debug.js"></script>
        <script src="${cp}/webjars/extjs/6.5.3/build/packages/ux/classic/ux-debug.js"></script>
        <script src="${cp}/webjars/extjs/6.5.3/build/packages/charts/classic/charts-debug.js"></script>
        <script src="${cp}/webjars/extjs/6.5.3/build/classic/theme-crisp/theme-crisp-debug.js"></script>
        <c:if test="${pageContext.request.getLocale().getLanguage() == 'it'}">
            <script src="${cp}/webjars/extjs/6.5.3/build/classic/locale/locale-it.js"></script>
        </c:if>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js" integrity="sha256-CutOzxCRucUsn6C6TcEYsauvvYilEniTXldPa6/wu0k=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/it.js" integrity="sha256-D8y560ZGsKY1LoAajKkQCG7y0Vkye361MH4yFv2K5kk=" crossorigin="anonymous"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.17/moment-timezone-with-data.min.js" integrity="sha256-h9eAuKJdF9lNAnZLJEX7ULhAfihRd0emy1Y4Bxqx7Js=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js" integrity="sha256-68s1Vjqw1KVP2DiR5uNilZQjf+tF6IrQI9PjKTY88nM=" crossorigin="anonymous"></script>

        <script src="${cp}/webjars/skeleton/js/ext-fixes.js"></script>
        <script src="${cp}/webjars/skeleton/js/_polyfill.js"></script>

        <link rel="stylesheet" href="${cp}/webjars/skeleton/css/ColoredFilteredColumn.css"/>
        <link rel="stylesheet" href="${cp}/webjars/skeleton/css/Treelistnav.css"/>

        <script src="${cp}/webjars/skeleton/js/AjaxProxyWithExceptionHandler.js"></script>        
        <script src="${cp}/webjars/skeleton/js/DetailsGrid.js"></script>
        <script src="${cp}/webjars/skeleton/js/DownloadButton.js"></script>
        <script src="${cp}/webjars/skeleton/js/DownloadCsvTool.js"></script>
        <link rel="stylesheet" href="${cp}/webjars/skeleton/css/InfoBox.css"/>
        <script src="${cp}/webjars/skeleton/js/InfoBox.js"></script>
        <script src="${cp}/webjars/skeleton/js/JsonTree.js"></script>
        <script src="${cp}/webjars/skeleton/js/SearchField.js"></script>
        <link rel="stylesheet" href="${cp}/webjars/skeleton/css/ToggleSlider.css"/>
        <script src="${cp}/webjars/skeleton/js/ToggleSlider.js"></script>
        <script src="${cp}/webjars/skeleton/js/datetime-renderers.js"></script>
        <script src="${cp}/webjars/skeleton/js/generic-renderers.js"></script>
        <script src="${cp}/webjars/skeleton/js/l10n.js"></script>


    </head>
    <body>
        <div>Nothing to see here</div>
    </body>
</html>
