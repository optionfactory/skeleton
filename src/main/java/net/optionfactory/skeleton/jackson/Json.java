package net.optionfactory.skeleton.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import java.io.IOException;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

public class Json {

    private static final ObjectMapper mapper = new Jackson2ObjectMapperBuilder()
            .failOnEmptyBeans(false)
            .modules(new ParameterNamesModule(), new Jdk8Module(), new JavaTimeModule())
            .autoDetectFields(true)
            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .featuresToDisable(JsonGenerator.Feature.FLUSH_PASSED_TO_STREAM)
            .build();

    public static String serialize(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> T deserialize(Class<T> clazz, String json) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static JsonNode deserializeAsTree(String json) {
        try {
            return mapper.readTree(json);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
