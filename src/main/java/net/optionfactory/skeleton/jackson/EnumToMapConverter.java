package net.optionfactory.skeleton.jackson;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import javassist.Modifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnumToMapConverter extends StdConverter<Enum, Map<String, Object>> {

    private final Logger logger = LoggerFactory.getLogger(EnumToMapConverter.class);

    @Override
    public Map<String, Object> convert(Enum value) {
        final HashMap<String, Object> result = new HashMap<>();
        result.put("enumName", value.name());
        result.put("enumOrdinal", value.ordinal());
        final Class dc = value.getDeclaringClass();
        for (Field field : dc.getDeclaredFields()) {
            try {
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                if (Modifier.isPrivate(field.getModifiers())) {
                    field.setAccessible(true);
                }
                final String fieldName = field.getName();
                final Object fieldValue = field.get(value);
                result.put(fieldName, fieldValue);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("Unable to map field {} for class {}", field.getName(), dc.getName());
            }
        }
        return result;
    }

}
