package net.optionfactory.skeleton.notifications;

public enum MessageType {
    SUCCESS, ERROR, WARNING

}
