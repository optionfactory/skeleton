package net.optionfactory.skeleton.hibernate;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.persistence.Entity;
import javax.tools.Diagnostic;
import org.hibernate.annotations.Immutable;
import org.hibernate.mapping.Table;
import org.hibernate.tool.schema.internal.DefaultSchemaFilter;
import org.hibernate.tool.schema.spi.SchemaFilter;
import org.hibernate.tool.schema.spi.SchemaFilterProvider;

@SupportedAnnotationTypes("javax.persistence.Entity")
@SupportedOptions({
    IgnorableEntitiesAnnotationProcessor.PACKAGE,
    IgnorableEntitiesAnnotationProcessor.PACKAGE_TO_SCAN
})
public class IgnorableEntitiesAnnotationProcessor extends AbstractProcessor {

    public static final String PACKAGE = "package";
    public static final String PACKAGE_TO_SCAN = "packageToScan";

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Ignore Immutable entities processor for hibernate database validation");
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment environment) {
        if (environment.processingOver() || annotations.isEmpty()) {
            return false;
        }

        final List<String> immutableEntitiesNames = environment.getElementsAnnotatedWith(Immutable.class)
                .stream()
                .filter(e -> e.getAnnotation(Immutable.class) != null)
                .map(e -> e.getAnnotation(Entity.class))
                .filter(e -> e != null)
                .map(e -> e.name())
                .collect(Collectors.toList());

        final Elements elements = processingEnv.getElementUtils();
        final String packageName = Optional.ofNullable(processingEnv.getOptions().get(PACKAGE)).orElseThrow(() -> new IllegalArgumentException("Missing destination package option"));
        final String packageToScanName = Optional.ofNullable(processingEnv.getOptions().get(PACKAGE_TO_SCAN)).orElseThrow(() -> new IllegalArgumentException("Missing package to scan option"));

        final FieldSpec immutableEntitiesList = FieldSpec.builder(ParameterizedTypeName.get(List.class, String.class), "exclude")
                .addModifiers(Modifier.PRIVATE, Modifier.FINAL)
                .initializer(String.format("$T.asList(%s)", listToString(immutableEntitiesNames)), Arrays.class)
                .build();

        final MethodSpec includeTable = MethodSpec.methodBuilder("includeTable")
                .addParameter(Table.class, "table")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(boolean.class)
                .addStatement("return !exclude.contains(table.getName())")
                .build();

        final TypeSpec schemaClass = TypeSpec.classBuilder(ClassName.get(packageName, "IgnorableEntitiesFilterSchema"))
                .addModifiers(Modifier.PUBLIC)
                .superclass(DefaultSchemaFilter.class)
                .addField(immutableEntitiesList)
                .addMethod(includeTable)
                .build();

        final FieldSpec schemaFilter = FieldSpec.builder(SchemaFilter.class, "sf", Modifier.PRIVATE, Modifier.FINAL)
                .initializer("new IgnorableEntitiesFilterSchema()")
                .build();

        final List<MethodSpec> interfaceMethods = Arrays.asList("getValidateFilter", "getMigrateFilter", "getDropFilter", "getCreateFilter")
                .stream()
                .map(methodName -> {
                    return MethodSpec.methodBuilder(methodName)
                            .addAnnotation(Override.class)
                            .addModifiers(Modifier.PUBLIC)
                            .returns(SchemaFilter.class)
                            .addStatement("return sf")
                            .build();
                })
                .collect(Collectors.toList());

        final TypeSpec filterClass = TypeSpec.classBuilder(ClassName.get(packageName, "IgnorableEntitiesFilter"))
                .addModifiers(Modifier.PUBLIC)
                .addSuperinterface(SchemaFilterProvider.class)
                .addField(schemaFilter)
                .addMethods(interfaceMethods)
                .build();

        try {
            JavaFile.builder(packageName, schemaClass)
                    .build()
                    .writeTo(processingEnv.getFiler());
            JavaFile.builder(packageName, filterClass)
                    .build()
                    .writeTo(processingEnv.getFiler());
        } catch (IOException ex) {
            throw new RuntimeException("Unable to write class", ex);
        }

        return false;
    }

    private String listToString(List<String> list) {
        return list.stream().map(e -> String.format("\"%s\"", e)).collect(Collectors.joining(", "));
    }

}
