package net.optionfactory.skeleton.hibernate;

import java.time.ZoneId;
import java.util.Comparator;
import org.hibernate.dialect.Dialect;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.LiteralType;
import org.hibernate.type.StringType;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

/*
In order for this to work, add a file called package-info.java in the same package scanned according to your SessionFactory configuration, e.g.:
    final LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource);
    builder.scanPackages(DatabaseConfig.class.getPackage().getName());
    [...]

package-info.java:

@TypeDef(name = "zoneid", defaultForType = ZoneId.class, typeClass = ZoneIdType.class)
package <...>;

import java.time.ZoneId;
import net.optionfactory.skeleton.hibernate.ZoneIdType;
import org.hibernate.annotations.TypeDef;


 */
public class ZoneIdType extends AbstractSingleColumnStandardBasicType<ZoneId>
        implements LiteralType<ZoneId> {

    public static final ZoneIdType INSTANCE = new ZoneIdType();

    public ZoneIdType() {
        super(VarcharTypeDescriptor.INSTANCE, ZoneIdTypeDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return "zoneid";
    }

    @Override
    protected boolean registerUnderJavaType() {
        return true;
    }

    @Override
    public String objectToSQLString(ZoneId value, Dialect dialect) throws Exception {
        return StringType.INSTANCE.objectToSQLString(value.getId(), dialect);
    }

    public static class ZoneIdTypeDescriptor extends AbstractTypeDescriptor<ZoneId> {

        public static final ZoneIdTypeDescriptor INSTANCE = new ZoneIdTypeDescriptor();

        public static class TimeZoneComparator implements Comparator<ZoneId> {

            public static final TimeZoneComparator INSTANCE = new TimeZoneComparator();

            @Override
            public int compare(ZoneId o1, ZoneId o2) {
                return o1.getId().compareTo(o2.getId());
            }
        }

        public ZoneIdTypeDescriptor() {
            super(ZoneId.class);
        }

        @Override
        public String toString(ZoneId value) {
            return value.getId();
        }

        @Override
        public ZoneId fromString(String string) {
            return ZoneId.of(string);
        }

        @Override
        public Comparator<ZoneId> getComparator() {
            return TimeZoneComparator.INSTANCE;
        }

        @Override
        public <X> X unwrap(ZoneId value, Class<X> type, WrapperOptions options) {
            if (value == null) {
                return null;
            }
            if (String.class.isAssignableFrom(type)) {
                return (X) toString(value);
            }
            throw unknownUnwrap(type);
        }

        @Override
        public <X> ZoneId wrap(X value, WrapperOptions options) {
            if (value == null) {
                return null;
            }
            if (String.class.isInstance(value)) {
                return fromString((String) value);
            }
            throw unknownWrap(value.getClass());
        }
    }

}
