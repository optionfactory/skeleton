package net.optionfactory.skeleton.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A DispatcherServlet with a custom noHandlerFound serializing the response in
 * JSON.
 */
public class JsonDispatcherServlet extends DispatcherServlet {

    private static final long serialVersionUID = 1L;
    private static final String MESSAGE = "[{'field': null, 'reason':'resource not found', 'metadata': null}]".replace('\'', '"');

    public JsonDispatcherServlet(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Override
    protected void noHandlerFound(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(MESSAGE);
    }
}
