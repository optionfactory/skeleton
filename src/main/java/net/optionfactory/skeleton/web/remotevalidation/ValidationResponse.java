package net.optionfactory.skeleton.web.remotevalidation;

public class ValidationResponse {

    public static ValidationResponse of(boolean valid, String reason) {
        final ValidationResponse res = new ValidationResponse();
        res.valid = valid;
        res.reason = reason;
        return res;
    }

    public static ValidationResponse valid() {
        return of(true, null);
    }

    public static ValidationResponse error(String reason) {
        return of(false, reason);
    }

    public boolean valid;
    public String reason;
}
