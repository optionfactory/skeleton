package net.optionfactory.skeleton.web;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Predicate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.savedrequest.RequestCache;

public class ServletPathDiscriminatorAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final Optional<RequestCache> requestCache;
    private final Predicate<String> discriminator;
    private final String requestDispatcherUrl;

    public ServletPathDiscriminatorAuthenticationEntryPoint(Optional<RequestCache> requestCache, Predicate<String> discriminator, String requestDispatcherUrl) {
        this.requestCache = requestCache;
        this.discriminator = discriminator;
        this.requestDispatcherUrl = requestDispatcherUrl;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        if (discriminator.negate().test(request.getServletPath())) {
            request.setAttribute("unauthorized", true);
            request.getRequestDispatcher(requestDispatcherUrl).forward(request, response);
            // call to forward() resets saved request to "/login", so we need to override it below
            // otherwise the user gets redirected to "/login" after login
            requestCache.ifPresent(rc -> rc.saveRequest(request, response));
        }
    }

}
