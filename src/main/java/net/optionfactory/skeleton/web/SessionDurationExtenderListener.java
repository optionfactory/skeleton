package net.optionfactory.skeleton.web;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/*
    Extends the duration of the session. Use (example):

    public class WebApp implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext sc) throws ServletException {
        sc.addListener(new SessionDurationExtenderListener(60 * 60 * 2));
    
    [...]
    
    }
 */
public class SessionDurationExtenderListener implements HttpSessionListener {

    private final int sessionDurationInSeconds;

    public SessionDurationExtenderListener(int sessionDurationInSeconds) {
        this.sessionDurationInSeconds = sessionDurationInSeconds;
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setMaxInactiveInterval(sessionDurationInSeconds);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
    }

}
