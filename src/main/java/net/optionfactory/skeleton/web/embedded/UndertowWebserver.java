package net.optionfactory.skeleton.web.embedded;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.resource.FileResourceManager;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.InstanceFactory;
import io.undertow.servlet.api.ServletContainerInitializerInfo;
import io.undertow.servlet.api.ServletStackTraces;
import io.undertow.servlet.util.ImmediateInstanceFactory;
import java.io.File;
import java.util.Collections;
import javax.servlet.ServletContainerInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UndertowWebserver implements Webserver {

    private final Logger logger = LoggerFactory.getLogger(UndertowWebserver.class);
    
    private final ServletContainerInitializer servletContainerInitializer;
    private final String listeningAddress;
    private final int listeningPort;
    private final String contextPath;
    private final WebappSource webappSource;
    
    private Undertow server;
    private DeploymentManager manager;

    public UndertowWebserver(ServletContainerInitializer servletContainerInitializer, String listeningAddress, int listeningPort, String contextPath, WebappSource webappSource) {
        this.servletContainerInitializer = servletContainerInitializer;
        this.listeningAddress = listeningAddress;
        this.listeningPort = listeningPort;
        this.contextPath = contextPath;
        this.webappSource = webappSource;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Preparing Undertow for webapp deployment");
        final InstanceFactory<? extends ServletContainerInitializer> instanceFactory = new ImmediateInstanceFactory<>(servletContainerInitializer);
        final DeploymentInfo deployment = new DeploymentInfo();
        deployment.setDeploymentName("default");
        deployment.setDisplayName("default");
        deployment.setServletStackTraces(ServletStackTraces.ALL);
        deployment.setContextPath(contextPath);
        deployment.setClassLoader(UndertowWebserver.class.getClassLoader());
        if (WebappSource.FILESYSTEM == webappSource) {
            deployment.setResourceManager(new FileResourceManager(new File("src/main/webapp")));
        }
        deployment.addServletContainerInitalizer(new ServletContainerInitializerInfo(WebAppServletContainerInitializer.class, instanceFactory, Collections.emptySet()));
        this.manager = Servlets.newContainer().addDeployment(deployment);
        this.manager.deploy();
        final HttpHandler httpHandler = Handlers.path().addPrefixPath(contextPath, manager.start());
        this.server = Undertow.builder().addHttpListener(listeningPort, listeningAddress).setHandler(httpHandler).build();
        this.server.start();
        logger.info("Started Undertow, listening at '{}:{}{}'. Webapp served from {}.", listeningAddress, listeningPort, contextPath, webappSource);
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Stopping Undertow...");
        this.server.stop();
        this.manager.stop();
        this.manager.undeploy();
        logger.info("Stopped Undertow. 👋");
    }

}
