package net.optionfactory.skeleton.web.embedded;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public interface Webserver extends InitializingBean, DisposableBean{
    
}
