package net.optionfactory.skeleton.web.embedded;

public enum WebappSource {
    FILESYSTEM(""),
    CLASSPATH("classpath:");

    private final String prefix;

    private WebappSource(String prefix) {
        this.prefix = prefix;
    }

    public String prefix(String path) {
        final String separator = path.startsWith("/")
                ? ""
                : "/";
        return String.format("%s%s%s", prefix, separator, path);
    }

}
