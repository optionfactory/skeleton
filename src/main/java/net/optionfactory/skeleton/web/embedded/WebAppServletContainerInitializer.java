package net.optionfactory.skeleton.web.embedded;

import java.util.Set;
import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class WebAppServletContainerInitializer implements ServletContainerInitializer {

    private final AnnotationConfigWebApplicationContext rootContext;
    private final WebApplication webApplication;

    public WebAppServletContainerInitializer(AnnotationConfigWebApplicationContext rootContext, WebApplication webApplication) {
        this.rootContext = rootContext;
        this.webApplication = webApplication;
    }

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext sc) throws ServletException {
        webApplication.setup(rootContext, sc);
    }

}
