package net.optionfactory.skeleton.web.embedded;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public interface WebApplication {

    void setup(AnnotationConfigWebApplicationContext rootContext, ServletContext sc) throws ServletException;

}
