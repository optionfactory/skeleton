package net.optionfactory.skeleton.web;

import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

public class InjectInModelInterceptor implements WebRequestInterceptor {

    private final String key;
    private final Object value;

    public InjectInModelInterceptor(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public void preHandle(WebRequest wr) throws Exception {
    }

    @Override
    public void postHandle(WebRequest wr, ModelMap mm) throws Exception {
        if (mm == null) {
            mm = new ModelMap();
        }
        mm.addAttribute(key, value);
    }

    @Override
    public void afterCompletion(WebRequest wr, Exception excptn) throws Exception {
    }
}
