package net.optionfactory.skeleton.time;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

public class ManualClock extends Clock {

    private final Clock inner;
    private Duration delta = Duration.ZERO;

    public ManualClock(Instant initial, ZoneId zoneId) {
        this.inner = Clock.fixed(initial, zoneId);
    }

    @Override
    public ZoneId getZone() {
        return inner.getZone();
    }

    @Override
    public ManualClock withZone(ZoneId zone) {
        return new ManualClock(inner.instant().plus(delta), zone);
    }

    @Override
    public Instant instant() {
        return inner.instant().plus(delta);
    }

    public void advance(Duration increment) {
        this.delta = this.delta.plus(increment);
    }

    public void reset() {
        delta = Duration.ZERO;
    }
}
