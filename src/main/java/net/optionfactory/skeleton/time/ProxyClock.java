package net.optionfactory.skeleton.time;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Optional;

public class ProxyClock extends Clock {

    private final Clock proxied;
    private Optional<Clock> override = Optional.empty();

    public ProxyClock(Clock regular) {
        this.proxied = regular;
    }

    @Override
    public ZoneId getZone() {
        return override.orElse(proxied).getZone();
    }

    @Override
    public ProxyClock withZone(ZoneId zone) {
        return new ProxyClock(override.orElse(proxied).withZone(zone));
    }

    @Override
    public Instant instant() {
        return override.orElse(proxied).instant();
    }

    public void setOverride(Clock clock) {
        override = Optional.of(clock);
    }

    public void removeOverride() {
        override = Optional.empty();
    }

}
